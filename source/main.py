#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Mar 15 14:21:44 2020

@author: Denis Nowotny
"""


import window


def main():
    
    print("Starting Level 89")
    window.Startwindow()
    print("Ending Level 89")
    
if __name__=="__main__":
    main()