#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Mar 15 14:53:13 2020

@author: Denis Nowotny
"""
import tkinter

def printinfo ():
    
    m_text = "\
    ************************\n\
    Autor: Denis Nowotny\n\
    Date: 22.03.20\n\
    Version: 0.2 beta\n\
    ************************"
    
    tkinter.messagebox.showinfo(message=m_text, title = "Infos")

