# Level 89

GUI Tool in Python for mp3gain in Linux

Current Version: 0.2-beta


# Install

`git clone https://gitlab.com/denoit00/level89.git`

`chmod -R 777 level89`

`cd level89`

`./starter.sh`

# Info

Pythonversion:	3.8

Plattform:	Linux

Developer:	Denis Nowotny, B.Eng

Licence: 	GPLv3

Languages:	en

# Requiered Packages:

mp3gain

python3

python3-pip

# Used Tools:

Python IDE:	spyder3

Texeditor: 	nano

# Structure

source		sourcecode

lang		languagePackages (later)

scripts		some Auxilary scripts (later)

pkg		Packages (later)
