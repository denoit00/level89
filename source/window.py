#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Mar 15 14:25:08 2020

@author: Denis Nowotny
"""

import tkinter

import info
import target
import value
import mp3gain

       
    

def Startwindow():
    
    
    def check_button():
        pathval=pathentry.get()
        statusval=target.pathstatus(pathval)
        statusbar.config(text=statusval)
    def start():
        pathval=pathentry.get()
        statusval=target.pathstatus(pathval)
        statusbar.config(text=statusval)
        print("starting")
        entry_value=dbfield.get()
        if(value.checkvalue(entry_value)==False):
            statusbar.config(text="Value invalide")
        else:
            statusbar.config(text=entry_value)
            targetval=entry_value
            status=mp3gain.checkparamter(statusval,pathval,targetval)
            if(status==True):
                statusbar.config(text="Sucessfull")
            else:
                statusbar.config(text="Failed")
            
        
    print("Opening Window")
    window = tkinter.Tk()
    window.title("Level 89")
    #menues
    menubar=tkinter.Menu(window)
    filemenu=tkinter.Menu(menubar, tearoff=0)
    helpmenu=tkinter.Menu(menubar, tearoff=0)
    
    helpmenu.add_command(label="Info",command=info.printinfo)
    menubar.add_cascade(label="Help",menu=helpmenu)
    
    window.config(menu=menubar)
    
    #inputfileds
    pathentrylabel= tkinter.Label(window,text="Enter Path: ")
    pathentry=tkinter.Entry(window,bd=5,width=80)
    checkbutton=tkinter.Button(window,text="Check Path",command=check_button)
    statusbar=tkinter.Label(window)
    dbfield=tkinter.Entry(window,bd=5, width=15)
    dbfiledinfo=tkinter.Label(window,text="dB")
    startbutton=tkinter.Button(window,text="Start",command=start)
    
    #layout
    pathentrylabel.grid(row=0,column=0)
    pathentry.grid(row=0,column=1,columnspan=4)
    checkbutton.grid(row=1,column=1)
    dbfield.grid(row=1,column=2)
    dbfiledinfo.grid(row=1,column=3)
    startbutton.grid(row=1,column=4)
    statusbar.grid(row=2,column=1,columnspan=4)
    
    window.mainloop()