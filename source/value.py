#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Mar 15 16:19:31 2020

@author: Denis Nowotny
"""

def checkvalue(inputval):
    try:
        inputval=int(inputval)
        return True
    except:
        return False
    
def convertvalue(inputvalue):
    return int(inputvalue)-89