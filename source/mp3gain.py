#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Mar 15 16:36:33 2020

@author: Denis Nowotny
"""
import target
import value

import subprocess
import time

def checkparamter(statusval:target.Pathstatus,pathval:str,targetval:int):
    
    if(statusval==target.Pathstatus.Folder):
        #print("Folder")
        return gainFolder(pathval,targetval)
    elif(statusval==target.Pathstatus.MP3):
        #print("MP3")
        return gainFile(pathval,targetval)
    else:
        return False

    
def gainFolder(targetpath,targetvalue):
    targetvalue=value.convertvalue(targetvalue)
    targetpath=target.folderpath(targetpath)
    text="yes|mp3gain -a -r -d "+str(targetvalue)+" "+targetpath+"*.mp3"
    print(text)
    proc = subprocess.Popen([text],stdin=subprocess.PIPE, shell=True)
    
    return True
    
    
def gainFile(targetpath,targetvalue):
    targetvalue=value.convertvalue(targetvalue)
    text="yes|mp3gain -r -d "+str(targetvalue)+" "+targetpath
    
    proc = subprocess.Popen([text],stdin=subprocess.PIPE, shell=True)
    time.sleep(1)
    
    return True




def debug(statusval:target.Pathstatus,pathval:str,targetval:int):
    print(statusval)
    print(pathval)
    print(targetval)
