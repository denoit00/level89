#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Mar 15 15:49:03 2020

@author: Denis Nowotny
"""

from enum import Enum
from os import path

class Pathstatus(Enum):
    Error=-1
    Empty=0
    File=1
    Folder=2
    MP3=3
    
def pathstatus(pathvalue):
    
    if(pathvalue==""):
        status=Pathstatus.Empty
    elif(path.isdir(pathvalue)):
        status=Pathstatus.Folder
    elif(path.isfile(pathvalue)):
        status=Pathstatus.File
        if(ismp3(pathvalue)==True):
            status=Pathstatus.MP3
    else:
        status=Pathstatus.Error
    
    return status

def ismp3(pathvalue:str):
    if(pathvalue.endswith(".mp3")):
        return True
    else:
        return False
    
def folderpath(pathvalue:str):
    if(pathvalue.endswith('/')):
        return pathvalue
    else:
        pathvalue=pathvalue+'/'
        return pathvalue